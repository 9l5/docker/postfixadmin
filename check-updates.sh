#!/bin/bash
#

checkApt()
{
    OUTPUT=$(apt update)
    pattern="^.*All packages are up to date.*$"

    if [[ $OUTPUT =~ $pattern ]]
    then
        echo "No updates available o/"
        return 0
    fi

    return 1
}

checkApt
aptUpdatesAvailable=$?

if [[ $aptUpdatesAvailable == 0 ]]
then
    echo "no updates available recognized"
else
    echo "should be updated! Triggering..."
    sh -s "curl --request POST --form token=$CI_JOB_TOKEN --form ref=$CI_COMMIT_BRANCH $MY_PAYLOAD $CI_API_V4_URL/projects/$CI_PROJECT_ID/trigger/pipeline"
fi
